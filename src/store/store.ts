import { combineReducers, configureStore } from '@reduxjs/toolkit';

const reportReducer = combineReducers({});

export const store = configureStore({
  reducer: {
    reports: reportReducer,
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
