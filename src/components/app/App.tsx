import './App.css';
import { Route, Routes } from 'react-router-dom';
import { Main } from '../main/Main';
import { Form } from '../form/Form';

function App() {
  return (
    <div className="app">
      <header className="app-header">
        <Routes>
          <Route path="/" element={<Main />} />
          <Route path="/form" element={<Form />} />
          <Route path="*" element={<Main />} />
        </Routes>
      </header>
    </div>
  );
}

export default App;
