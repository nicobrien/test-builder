import React from 'react';
import { render, screen } from '@testing-library/react';
import { Form } from './Form';
import { MemoryRouter } from 'react-router-dom';

test('renders form', () => {
  // wrap in router https://v5.reactrouter.com/web/guides/testing
  render(
    <MemoryRouter>
      <Form />
    </MemoryRouter>,
  );
  const linkElement = screen.getByText('form');
  expect(linkElement).toBeInTheDocument();
});
