import { useState } from 'react';
import { Link } from 'react-router-dom';

export const Form = (): JSX.Element => {
  const [count, _] = useState(0);

  return (
    <div>
      <p id="form">form</p>
      <Link className="app-link" to="/">
        home {count}
      </Link>
    </div>
  );
};
