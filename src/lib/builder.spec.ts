import fc from 'fast-check';
import { Builder } from './builder';
import { primitive, Primitive } from './spec.common';

describe('Builder', () => {
  describe('create', () => {
    it('should be able to create empty objects', () => {
      const b = new Builder<{}>(() => ({}));
      expect(b.create()).toEqual({});
    });

    it('should be able to create a record with one entry', () => {
      fc.assert(
        fc.property(fc.string(), fc.anything(), (key: string, x: any) => {
          const b = new Builder<Record<string, number>>(() => ({
            [key]: x,
          }));
          expect(b.create()[key]).toEqual(x);
        }),
      );
    });

    it('should be able to create records', () => {
      fc.assert(
        fc.property(
          fc.dictionary(fc.string(), fc.anything()),
          (x: Record<string, any>) => {
            const b = new Builder<Record<string, any>>(() => x);
            expect(b.create()).toEqual(x);
          },
        ),
      );
    });
  });

  describe('merge', () => {
    it('should have empty object be identity', () => {
      fc.assert(
        fc.property(
          fc.dictionary(fc.string(), fc.anything()),
          (x: Record<string, any>) => {
            const b = new Builder<Record<string, any>>(() => x);
            expect(b.with({}).create()).toEqual(x);
          },
        ),
      );
    });

    it('should be idempotent for primitives', () => {
      fc.assert(
        fc.property(
          fc.dictionary(fc.string(), primitive),
          primitive,
          (x: Record<string, Primitive>, v: Primitive) => {
            fc.pre(Object.keys(x).length > 0);

            const k1 = Object.keys(x)[0];
            const b = new Builder<Record<string, Primitive>>(() => x);

            expect(
              b
                .with({ [k1]: v })
                .with({ [k1]: v })
                .create(),
            ).toEqual(b.with({ [k1]: v }).create());
          },
        ),
      );
    });

    it('should be able to merge primitives given matching keys', () => {
      fc.assert(
        fc.property(primitive, primitive, (x: Primitive, y: Primitive) => {
          const b = new Builder<Record<string, Primitive>>(() => ({
            k1: x,
          }));
          expect(b.with({ k1: y }).create()).toEqual({ k1: y });
        }),
      );
    });

    it('should be able to merge primitives given different keys', () => {
      fc.assert(
        fc.property(primitive, primitive, (x: Primitive, y: Primitive) => {
          const b = new Builder<Record<string, Primitive>>(() => ({
            k1: x,
          }));
          expect(b.with({ k2: y }).create()).toEqual({ k1: x, k2: y });
        }),
      );
    });

    it('should be able to merge nested primitives', () => {
      fc.assert(
        fc.property(primitive, primitive, (x: Primitive, y: Primitive) => {
          const b = new Builder<Record<string, Record<string, Primitive>>>(
            () => ({ k1: { k2: x } }),
          );
          expect(b.with({ k1: { k3: y } }).create()).toEqual({
            k1: { k2: x, k3: y },
          });
        }),
      );
    });
  });

  describe('oust', () => {
    it('should be able to replace nested number properties', () => {
      fc.assert(
        fc.property(primitive, primitive, (x: Primitive, y: Primitive) => {
          const b = new Builder<Record<string, Record<string, Primitive>>>(
            () => ({ k1: { k2: x } }),
          );
          expect(b.oust({ k1: { k3: y } }).create()).toEqual({ k1: { k3: y } });
        }),
      );
    });

    type TestQ = Record<string, Record<string, object | Primitive>>;

    it('should replace given target has nested object but source has nested primitive', () => {
      fc.assert(
        fc.property(
          fc.object(),
          primitive,
          (target: object, source: Primitive) => {
            const b = new Builder<TestQ>(() => ({ k1: { k2: target } }));
            expect(b.oust({ k1: { k3: source } }).create()).toEqual({
              k1: { k3: source },
            });
          },
        ),
      );
    });

    it('should merge intermediate objects given source has nested object', () => {
      fc.assert(
        fc.property(
          fc.object(),
          fc.object(),
          primitive,
          (target: object, s1: object, s2: Primitive) => {
            const b = new Builder<TestQ>(() => ({ k1: { k2: target } }));
            expect(b.oust({ k1: { k3: s1, k4: s2 } }).create()).toEqual({
              k1: { k2: target, k3: s1, k4: s2 },
            });
          },
        ),
      );
    });
  });

  describe('freeze', () => {
    it('should be able to freeze empty objects', () => {
      const b = new Builder<{}>(() => ({}));
      expect(b.freeze()).toEqual({});
    });

    it('should be able to freeze a record with one entry', () => {
      fc.assert(
        fc.property(fc.string(), fc.anything(), (key: string, x: any) => {
          const b = new Builder<Record<string, number>>(() => ({
            [key]: x,
          }));
          expect(b.freeze()[key]).toEqual(x);
        }),
      );
    });

    it('should be able to freeze records', () => {
      fc.assert(
        fc.property(
          fc.dictionary(fc.string(), fc.anything()),
          (x: Record<string, any>) => {
            const b = new Builder<Record<string, any>>(() => x);
            expect(b.freeze()).toEqual(x);
          },
        ),
      );
    });
  });
});
