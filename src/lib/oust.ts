import { O, canRecurse } from './functions';

// todo return type
export const oust = <T extends O, S extends O>(target: T, source: S): any => {
  const result: O = { ...target };

  Object.keys(source).forEach((key) => {
    if (Array.isArray(result[key]) && Array.isArray(source[key])) {
      result[key] = source[key];
    } else if (
      canRecurse(result[key]) &&
      canRecurse(source[key]) &&
      // if at least one of the properties on source is recursable,
      // otherwise we replace the entire leaf
      Object.values(source[key]).some(canRecurse)
    ) {
      result[key] = oust(result[key] as O, source[key] as O);
    } else {
      result[key] = source[key];
    }
  });

  return result;
};
