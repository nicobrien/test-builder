import fc from 'fast-check';
import { canRecurse } from './functions';

describe('canRecurse', () => {
  it('should be true for records', () => {
    fc.assert(
      fc.property(
        fc.dictionary(fc.string(), fc.anything()),
        (x: Record<string, any>) => {
          expect(canRecurse(x)).toBe(true);
        },
      ),
    );
  });

  it('should be false when not object', () => {
    fc.assert(
      fc.property(
        fc.oneof(
          fc.integer(),
          fc.string(),
          fc.boolean(),
          fc.date(),
          fc.array(fc.anything()),
        ),
        (n: number | string | boolean | Date | any[]) => {
          expect(canRecurse(n)).toBe(false);
        },
      ),
    );
  });
});
