import { O, isObject } from './functions';
import { ReadonlyRecursive } from './types';

export const deepFreeze = <T extends O>(o: T): ReadonlyRecursive<T> => {
  const propNames = Object.getOwnPropertyNames(o);

  for (const name of propNames) {
    const value = o[name];

    if (isObject(value)) {
      deepFreeze(value);
    }
  }

  return Object.freeze(o);
};
