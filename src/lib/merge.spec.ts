import fc from 'fast-check';
import { merge } from './merge';
import { primitive, Primitive } from './spec.common';

describe('merge', () => {
  it('should return the empty object given empty object params', () => {
    expect(merge({}, {})).toEqual({});
  });

  it('should overwrite target given identical key and target value is a primitive', () => {
    fc.assert(
      fc.property(
        fc.string(),
        primitive,
        fc.anything(),
        (key: string, n: Primitive, m: any) => {
          expect(merge({ [key]: n }, { [key]: m })).toEqual({ [key]: m });
        },
      ),
    );
  });

  it('should overwrite target given identical key and target value is a primitive when nested', () => {
    fc.assert(
      fc.property(
        fc.string(),
        fc.string(),
        primitive,
        fc.anything(),
        (k1: string, k2: string, n: Primitive, m: any) => {
          expect(merge({ [k1]: { [k2]: n } }, { [k1]: { [k2]: m } })).toEqual({
            [k1]: { [k2]: m },
          });
        },
      ),
    );
  });

  it('should keep both entries given different keys', () => {
    fc.assert(
      fc.property(
        fc.string(),
        fc.string(),
        fc.anything(),
        fc.anything(),
        (k1: string, k2: string, n: any, m: any) => {
          fc.pre(k1 !== k2);

          expect(merge({ [k1]: n }, { [k2]: m })).toEqual({ [k1]: n, [k2]: m });
        },
      ),
    );
  });

  it('should merge nested object with different keys', () => {
    fc.assert(
      fc.property(
        fc.string(),
        fc.string(),
        fc.anything(),
        fc.anything(),
        (k1: string, k2: string, n: any, m: any) => {
          fc.pre(k1 !== k2);

          expect(merge({ k: { [k1]: n } }, { k: { [k2]: m } })).toEqual({
            k: { [k1]: n, [k2]: m },
          });
        },
      ),
    );
  });

  it('should be idempotent in the second argument given not an array', () => {
    fc.assert(
      fc.property(
        fc.dictionary(fc.string(), primitive),
        fc.dictionary(fc.string(), primitive),
        (o1: Record<string, Primitive>, o2: Record<string, Primitive>) => {
          const f = (o: Record<string, Primitive>) => merge(o1, o);

          expect(f(o1)).toEqual(f(f(o1)));
        },
      ),
    );
  });

  it('should take target given an empty source', () => {
    fc.assert(
      fc.property(
        fc.dictionary(fc.string(), fc.anything()),
        (o: Record<string, any>) => {
          expect(merge(o, {})).toEqual(o);
        },
      ),
    );
  });

  it('should take source given empty target', () => {
    fc.assert(
      fc.property(
        fc.dictionary(fc.string(), fc.anything()),
        (o: Record<string, any>) => {
          expect(merge({}, o)).toEqual(o);
        },
      ),
    );
  });

  it('should be associative for non objects', () => {
    fc.assert(
      fc.property(
        fc.dictionary(fc.string(), primitive),
        fc.dictionary(fc.string(), primitive),
        fc.dictionary(fc.string(), primitive),
        (
          x: Record<string, Primitive>,
          y: Record<string, Primitive>,
          z: Record<string, Primitive>,
        ) => {
          expect(merge(x, merge(y, z))).toEqual(merge(merge(x, y), z));
        },
      ),
    );
  });

  it('should be idempotent given value is not array', () => {
    fc.assert(
      fc.property(
        fc.dictionary(fc.string(), primitive),
        (o: Record<string, Primitive>) => {
          expect(merge(o, o)).toEqual(o);
        },
      ),
    );
  });

  it('should take undefined from source', () => {
    fc.assert(
      fc.property(fc.string(), primitive, (key: string, o: Primitive) => {
        expect(merge({ [key]: o }, { [key]: undefined })).toEqual({
          [key]: undefined,
        });
      }),
    );
  });

  it('should take null from source', () => {
    fc.assert(
      fc.property(fc.string(), primitive, (key: string, o: Primitive) => {
        expect(merge({ [key]: o }, { [key]: null })).toEqual({
          [key]: null,
        });
      }),
    );
  });

  describe('arrays', () => {
    it('should include source and target elements', () => {
      fc.assert(
        fc.property(
          fc.string(),
          fc.array(fc.string()),
          fc.array(fc.string()),
          (key: string, xs: string[], ys: string[]) => {
            expect(merge({ [key]: xs }, { [key]: ys })).toEqual({
              [key]: expect.arrayContaining(xs),
            });
            expect(merge({ [key]: xs }, { [key]: ys })).toEqual({
              [key]: expect.arrayContaining(ys),
            });
          },
        ),
      );
    });

    it('should produce the union of two distinct arrays', () => {
      fc.assert(
        fc.property(
          fc.string(),
          fc.uniqueArray(fc.string()),
          (key: string, zs: string[]) => {
            const i = zs.length / 2;
            const xs = zs.slice(0, i);
            const ys = zs.slice(-(i + 1));
            expect(merge({ [key]: xs }, { [key]: ys })).toEqual({ [key]: zs });
          },
        ),
      );
    });
  });

  describe('mutability', () => {
    const o = Object.freeze({
      a: 'a',
      b: {
        b1: 1,
        b2: ['p', 'q', 'r'],
        b3: new Date('1995-12-17T03:24:00'),
      },
      c: { c1: true },
    });

    it('should not mutate the source', () => {
      fc.assert(
        fc.property(
          fc.dictionary(fc.string(), fc.anything()),
          (t: Record<string, any>) => {
            merge(t, o);
            expect(o).toEqual({
              a: 'a',
              b: {
                b1: 1,
                b2: ['p', 'q', 'r'],
                b3: new Date('1995-12-17T03:24:00'),
              },
              c: { c1: true },
            });
          },
        ),
      );
    });

    it('should not mutate the target', () => {
      fc.assert(
        fc.property(
          fc.dictionary(fc.string(), fc.anything()),
          (s: Record<string, any>) => {
            merge(o, s);
            expect(o).toEqual({
              a: 'a',
              b: {
                b1: 1,
                b2: ['p', 'q', 'r'],
                b3: new Date('1995-12-17T03:24:00'),
              },
              c: { c1: true },
            });
          },
        ),
      );
    });
  });
});
