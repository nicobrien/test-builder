import { O, canRecurse } from './functions';

// todo return type
export const merge = <T extends O, S extends O>(target: T, source: S): any => {
  const result: O = { ...target };

  Object.keys(source).forEach((key) => {
    if (Array.isArray(result[key]) && Array.isArray(source[key])) {
      result[key] = Array.from(
        new Set((result[key] as unknown[]).concat(source[key])),
      );
    } else if (canRecurse(result[key]) && canRecurse(source[key])) {
      result[key] = merge(result[key] as O, source[key] as O);
    } else {
      result[key] = source[key];
    }
  });

  return result;
};
