import fc, { Arbitrary } from 'fast-check';

export type Primitive = string | number | boolean | Date;

export const primitive: Arbitrary<Primitive> = fc.oneof(
  fc.string(),
  fc.integer(),
  fc.double(),
  fc.boolean(),
  fc.date(),
);
