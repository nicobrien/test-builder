export type KeysMatching<T, U> = {
  [K in keyof T]: T[K] extends U ? K : never;
}[keyof T];

export type PartialRecursive<T> = {
  [K in keyof T]?: T[K] extends (infer U)[]
    ? PartialRecursive<U>[]
    : T[K] extends object
    ? PartialRecursive<T[K]>
    : T[K];
};
type ReadonlyRecursive<T> = {
  readonly [K in keyof T]: ReadonlyRecursive<T[K]>;
};
