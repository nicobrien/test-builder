export const canRecurse = (o: any): boolean =>
  isObject(o) && !isDate(o) && !Array.isArray(o);

export const isObject = (o: any): o is object => {
  return !!o && typeof o === 'object';
};

export const isDate = (d: any): d is Date => {
  return !!d && Object.prototype.toString.call(d) === '[object Date]';
};

export type O = Record<string, any>;
