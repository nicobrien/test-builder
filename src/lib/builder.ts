import { merge } from './merge';
import { deepFreeze } from './deepFreeze';
import { oust } from './oust';
import { KeysMatching, PartialRecursive, ReadonlyRecursive } from './types';

/**
 * {@link Builder} is a generic GoF builder implementation
 * intended for test-data generation. It provides a fluent
 * interface with the goal of making test arrangement easier.
 *
 * The main idea is allow consumers write a general purpose
 * object template for their object under test and then
 * use the {@link Builder} to specify only those properties
 * that are important to a test's outcome during the test
 * arrangement phase.
 *
 * @example
 * ```typescript
 *   const b = new Builder<Dto>(() => ({
 *     id: 1,
 *     val: 'some-v',
 *     obj: {
 *       x: 'some-x',
 *       y: 'some-y'
 *     },
 *     lst: [1, 2, 3],
 *   }));
 *
 *   const d1 = b.with({ id: 2 }).create();
 *   const d2 = b.with({ val: 'another-v' }).create();
 * ```
 */
export class Builder<T> {
  public constructor(private readonly template: () => T) {}

  /**
   * Override properties of T at arbitrary depth.
   *
   * @example
   * ```typescript
   *   const d3 = b
   *     .with({ id: 3 })
   *     .with({ obj: { x: 'a-different-x' } })
   *     .create();
   * ```
   *
   * @param partial a {@link PartialRecursive} slice of T
   */
  public with(partial: PartialRecursive<T>): Builder<T> {
    return new Builder<T>(() => merge(this.template(), partial));
  }

  /**
   * Replace properties of T at arbitrary depth.
   *
   * @example
   * ```typescript
   *   const d3 = b
   *     .oust({ obj: { z: 'some-z' } })
   *     .create();
   * ```
   *
   * @param partial a {@link PartialRecursive} slice of T
   */
  public oust(partial: PartialRecursive<T>): Builder<T> {
    return new Builder<T>(() => oust(this.template(), partial));
  }

  /**
   * A convenience method for composing builders. The output
   * of the builder parameter will be used to replace T[K]
   * on this builder.
   *
   * @example
   * ```typescript
   *   const objectBuilder = new Builder<{ x: string, y: string }>(() => ({
   *     x: 'x', y: 'y'
   *   }));
   *
   *   const d4 = b.use('obj', objectBuilder).create();
   * ```
   *
   * @param key the  key on the object T that the value
   * produced by the parameterized builder should target.
   * @param builder a builder of type U that extends T[K]
   */
  public use<U extends object, K extends KeysMatching<T, U>>(
    key: K,
    builder: Builder<U>,
  ): Builder<T> {
    return new Builder<T>(() =>
      merge(this.template(), { [key]: builder.create() }),
    );
  }

  /**
   * @returns the target object of type T with all overrides merged.
   */
  public create(): T {
    return this.template();
  }

  /**
   * A version of {@link create} where all properties are
   * `readonly`.
   *
   * @example
   * ```typescript
   *   const c = new Builder<{ x: { y: string; } }>(() => ({
   *     x: { y: 'y' }
   *   }));
   *
   *   const d5 = c.freeze();
   *   // Cannot assign to y because it is a readonly property
   *   d5.x.y = 'z'; // error
   * ```
   */
  public freeze(): ReadonlyRecursive<T> {
    return deepFreeze(this.create());
  }
}
