import { deepFreeze } from './deepFreeze';

describe('deepFreeze', () => {
  it('should freeze nested objects', () => {
    const x: any = deepFreeze({
      k: { k1: { k2: 'v' } },
    });

    expect(() => {
      x.k.k1 = { k2: 'w' };
    }).toThrow();

    expect(() => {
      x.k.k1.k2 = 'w';
    }).toThrow();
  });
});
