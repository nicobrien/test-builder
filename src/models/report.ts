import { ReportItem } from './report-item';

export type Report = {
  items: ReportItem[];
  created: Date;
  updated: Date;
  assessorName: string;
  subjectName: string;
};
