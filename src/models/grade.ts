export type Grade = StandardGrade | CustomGrade;

export type StandardGrade =
  | 'C-'
  | 'C'
  | 'C+'
  | 'B-'
  | 'B'
  | 'B+'
  | 'A-'
  | 'A'
  | 'A+';

export type CustomGrade = {
  displayName: string;
  mappedGrade: StandardGrade;
};
