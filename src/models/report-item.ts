import { Comments } from './comments';
import { Grade } from './grade';

export type ReportItem = {
  subject: string;
  grade: Grade;
  comments: Comments;
};
